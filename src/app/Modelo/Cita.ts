export class Cita{
    motivoCita: string;
    fechaHora:  Date;
    paciente:   string;
    medico:     string;
    diagnostico: any[];

    constructor(motivoCita: string, fechaHora: Date, paciente: string, medico: string){
        this.motivoCita = motivoCita;
        this.fechaHora = fechaHora;
        this.paciente = paciente;
        this.medico = medico;
    }
}