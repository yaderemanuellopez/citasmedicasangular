export class Paciente{
    dni:        string;
    usuario:    string;
    clave:      string;
    nombre:     string;
    apellidos:  string;
    nss:        string;
    numTarjeta: string;
    telefono:   string;
    direccion:  string;
    citas:      any[];
    medicos:    any[];

    constructor(    dni: string, usuario: string, clave: string, nombre: string, apellidos: string, nss: string, numTarjeta: string, telefono: string, direccion: string){
        
        this.dni = dni;
        this.usuario = usuario;
        this.clave = clave;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nss = nss;
        this.numTarjeta = numTarjeta,
        this.telefono = telefono;
        this.direccion = direccion;
    }
}