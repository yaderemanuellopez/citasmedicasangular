export class Medico{
    dni: string;
	usuario: string;
	clave: string;
	nombre: string;
	apellidos: string;
	numColegiado: string;
	especialidad: string;
    citas:      any[];
    pacientes:    any[];

    constructor(    dni: string, usuario: string, clave: string, nombre: string, apellidos: string, numColegiado: string, especialidad: string){
        
        this.dni = dni;
        this.usuario = usuario;
        this.clave = clave;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.numColegiado = numColegiado;
        this.especialidad = especialidad;
    }
}