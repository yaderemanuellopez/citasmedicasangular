import { Component, OnInit } from '@angular/core';
import { Medico } from 'src/app/Modelo/Medico';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-lista-medicos',
  templateUrl: './lista-medicos.component.html',
  styleUrls: ['./lista-medicos.component.css']
})
export class ListaMedicosComponent implements OnInit {

  listaMedicos: Medico[] = [];

  constructor(private servicios: ServiceService ) { }

  ngOnInit(): void {
    this.listarMedicos();
  }

  listarMedicos(){
    this.servicios.getMedicos().subscribe(data => {
      console.log(data);
      this.listaMedicos = data;
    }, error => {console.log(error);} );
  }
}
