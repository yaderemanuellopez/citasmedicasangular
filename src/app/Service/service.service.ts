import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  UrlMedico='http://localhost:8080/medicos'
  UrlPaciente='http://localhost:8080/pacientes'
  UrlCita='http://localhost:8080/citas'
  UrlDiagnostico='http://localhost:8080/diagnosticos'

  getMedicos() : Observable<any>{
    return this.http.get(this.UrlMedico);
  }

  getPacientes() : Observable<any>{
    return this.http.get(this.UrlPaciente);
  }

  getCitas() : Observable<any>{
    return this.http.get(this.UrlCita);
  }

  getDiagnosticos() : Observable<any>{
    return this.http.get(this.UrlDiagnostico);
  }

}
