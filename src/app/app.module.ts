import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/header/header.component';
import { FooterComponent } from './Components/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AddMedicoComponent } from './Medico/add-medico/add-medico.component';
import { ListaMedicosComponent } from './Medico/lista-medicos/lista-medicos.component';
import { AddPacienteComponent } from './Paciente/add-paciente/add-paciente.component';
import { ListaPacientesComponent } from './Paciente/lista-pacientes/lista-pacientes.component';
import { AddCitaComponent } from './Cita/add-cita/add-cita.component';
import { ListaCitasComponent } from './Cita/lista-citas/lista-citas.component';
import { HttpClientModule } from '@angular/common/http';
import { ButtonsComponent } from './Components/buttons/buttons.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AddMedicoComponent,
    ListaMedicosComponent,
    AddPacienteComponent,
    ListaPacientesComponent,
    AddCitaComponent,
    ListaCitasComponent,
    ButtonsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
