import { Component, OnInit, Injectable } from '@angular/core';
import { Paciente } from 'src/app/Modelo/Paciente';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-lista-pacientes',
  templateUrl: './lista-pacientes.component.html',
  styleUrls: ['./lista-pacientes.component.css']
})
export class ListaPacientesComponent implements OnInit {

  constructor(private servicios: ServiceService) { }ç

  listaPacientes: Paciente[] = [];

  ngOnInit(): void {
    this.listarPacientes();
  }

  listarPacientes(){
    this.servicios.getPacientes().subscribe(data => {
      console.log(data);
      this.listaPacientes = data;
    }, error => {console.log(error);} );
  }

}
