import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCitaComponent } from './Cita/add-cita/add-cita.component';
import { ListaCitasComponent } from './Cita/lista-citas/lista-citas.component';
import { ButtonsComponent } from './Components/buttons/buttons.component';
import { AddMedicoComponent } from './Medico/add-medico/add-medico.component';
import { ListaMedicosComponent } from './Medico/lista-medicos/lista-medicos.component';
import { AddPacienteComponent } from './Paciente/add-paciente/add-paciente.component';
import { ListaPacientesComponent } from './Paciente/lista-pacientes/lista-pacientes.component';

const routes: Routes = [
  {path: '', component: ButtonsComponent},
  {path: 'registroMedico', component: AddMedicoComponent},
  {path: 'registroPaciente', component: AddPacienteComponent},
  {path: 'listarMedicos', component: ListaMedicosComponent},
  {path: 'listarPacientes', component: ListaPacientesComponent},
  {path: 'listarCitas', component: ListaCitasComponent},
  {path: 'nuevaCita', component: AddCitaComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
