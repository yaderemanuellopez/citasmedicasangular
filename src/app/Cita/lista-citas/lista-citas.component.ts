import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Cita } from 'src/app/Modelo/Cita';

@Component({
  selector: 'app-lista-citas',
  templateUrl: './lista-citas.component.html',
  styleUrls: ['./lista-citas.component.css']
})
export class ListaCitasComponent implements OnInit {

  listaCitas: Cita[]=[];

  constructor( private servicios: ServiceService) { }

  ngOnInit(): void {
    this.listarCitas();
  }

  listarCitas(){
    this.servicios.getCitas().subscribe(data => {
      console.log(data);
      this.listaCitas = data;
    }, error => {console.log(error);} );
  }
}
